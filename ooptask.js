// ooptask.js

class Table {
  constructor(init) {
    this.init = init;
  }

  createHeader(Data) {
    let open = "<thead> <tr>";
    let close = "</tr> </thead>";
      
      Data.forEach((d) => { 
        open += `<th> ${d} </th>`;
      });

    return open + close;
  }

  createBody(Data) {
    let open = "<tbody>";
    let close = "</tbody>";

    Data.forEach((d) => {
      open += `
        <tr>
          <td> ${d[0]} </td>
          <td> ${d[1]} </td>
          <td> ${d[2]} </td>
        </tr>
      `;
    });

    return open + close;
  }

  render(element) {
    let tableLibrary = "<table class='table table-hover'>" +
        this.createHeader (this.init.columns) +
        this.createBody (this.init.Data) +
        "</table>";
    element.innerHTML = tableLibrary;
    // element.addEventListener('mouseover',someFunction);
  }
}

const tableLibrary = new Table({
  columns: [`Name`, `Email`, `Address`],
  Data: [

      [`Wina`, `winaaz@gmail.com`, `Bandung`],
      [`Azha`, `azha123@gmail.com`, `Jakarta`],
  ]
});


const app = document.getElementById("app");
tableLibrary.render(app);
